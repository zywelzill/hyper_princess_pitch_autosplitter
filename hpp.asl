﻿state("pitch gm7") {
	//Sin uso
	//byte uiState :  "pitch gm7.exe", 0x0019EB5C;

	//Determina el estado del juego (4 = menú, 0 = transición, 5 = en juego) NO ESTÁ DEFINIDO
	byte level : "jbfmod.dll", 0x0000E890, 0x138, 0xD8, 0x108, 0x674, 0x504;

	//Determina la transición de pantalla. (0 = sin transición, 1 = en transición) NO ESTÁ DEFINIDO
	byte transition : "pitch gm7.exe", 0x0018B624, 0x218, 0xE0, 0x544, 0x80, 0x1CC;

	//Representa la opción seleccionada en los menús.
	byte menuOption : "pitch gm7.exe", 0x001AF2F4, 0x6C, 0x0, 0x10C, 0x4, 0x17C;

	//La vida del jefe, aparece en el cuarto del jefe (revisar: valores extraños luego de derrotar a los jefes).
	byte bossHealth : "pitch gm7.exe", 0x001AF2F4, 0x6C, 0xB8, 0x10C, 0x4, 0x8C;
}

init {
	vars.begin = true;
	vars.cont = 0;
	vars.wait = false;
}

start {
	if (current.level == 0 && old.level == 4 && current.transition == 1) {
		vars.begin = true;
		return true;
	}
}

split {
	if (current.transition == 1 && !vars.wait) {
		vars.wait = true;

		if (vars.begin) {
			vars.begin = false;
			return false;
		} else {
			return true;
		}

	} else if (current.transition == 0 && vars.wait) {
		vars.wait = false;
	}
}

reset
{
    //return (current.level == 4 && current.transition == 0);
}